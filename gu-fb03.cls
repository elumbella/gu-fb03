%!TEX program = xelatex
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{gu-fb03}[2018/02/10 Class for FB03 at Goethe University Frankfurt, Germany]
%% Dies ist die Klasse für Hausarbeiten am FB03 an der Goethe-Universität Frankfurt
% es handelt sich um eine inoffizielle Version
% use at your own risk
% Savesym laden, um die eigene titlepage zu verwenden, statt der aus der article Klasse
\RequirePackage{savesym}
% kvoptions für die Weitergabe der Sprache an andere Pakete und Bestimmung des Typs
\RequirePackage{kvoptions}
% Initiale Vorbereitungen
\SetupKeyvalOptions{%
	family = GUFB,%
	prefix = GUFB@%
}
% "language" als Paket-Option deklarieren
\DeclareStringOption[]{language}
%% Zusätzliche Angaben zu Autor und Dokument, hauptsächlich für die Titelseite
% Abschluss, bzw. Studiengang (z.B. BA Politikwissenschaften)
\newcommand\degree[1]{\renewcommand\@degree{#1}}
\newcommand\@degree{\@latex@error{No \noexpand\degree given}\@ehc}
% Matrikelnummer
\newcommand\ID[1]{\renewcommand\@ID{#1}}
\newcommand\@ID{\@latex@error{No \noexpand\ID given}\@ehc}
% E-Mail-Adresse
\newcommand\email[1]{\renewcommand\@email{#1}}
\newcommand\@email{\@latex@error{No \noexpand\email given}\@ehc}
% Postadresse
\newcommand\address[1]{\renewcommand\@address{#1}}
\newcommand\@address{\@latex@error{No \noexpand\address given}\@ehc}
% Titelseite und Erklärung zur Prüfungsleistung je nach angegebenem Typ auswählen
\DeclareVoidOption{assignment}{%
	\newcommand{\maketitle}{%
		\input{titlepage-assignment}%
	}%
	\newcommand{\erklpruf}{%
		\input{erklpruf}%
	}%
	%% Felder für Hausarbeiten (assignment)
	% Veranstaltungsname
	\newcommand\class[1]{\renewcommand\@class{##1}}
	\newcommand\@class{\@latex@error{No \noexpand\class given}\@ehc}
	% Dozent*in
	\newcommand\teacher[1]{\renewcommand\@teacher{##1}}
	\newcommand\@teacher{\@latex@error{No \noexpand\teacher given}\@ehc}
	% Semester (z.B. Wintersemester 2017/18)
	\newcommand\term[1]{\renewcommand\@term{##1}}
	\newcommand\@term{\@latex@error{No \noexpand\term given}\@ehc}
}
\DeclareVoidOption{thesis}{%
	\newcommand{\maketitle}{%
		\input{titlepage-thesis}%
	}
	\newcommand{\erklpruf}{%
		\input{erklpruf}%
	}
	%% Felder für Abschlussarbeiten
	% Erstbetreuung
	\newcommand\firstsupervisor[1]{\renewcommand\@firstsupervisor{##1}}
	\newcommand\@firstsupervisor{\@latex@error{No \noexpand\firstsupervisor given}\@ehc}
	% Zweitbetreuung
	\newcommand\secondsupervisor[1]{\renewcommand\@secondsupervisor{##1}}
	\newcommand\@secondsupervisor{\@latex@error{No \noexpand\secondsupervisor given}\@ehc}
}
\DeclareVoidOption{proposal}{%
	\newcommand{\maketitle}{%
		\input{titlepage-proposal}
	}
	\newcommand{\erklpruf}{%
		% keine Erklärung zur Prüfungsleistung bei einem Expose
		%\input{erklpruf}
	}
}
\DeclareVoidOption{internship}{%
	\newcommand{\maketitle}{%
		\input{titlepage-internship}
	}%
	\newcommand{\erklpruf}{%
		\input{erklpruf}
	}%
	%% Felder für Hausarbeiten (assignment)
}
% Alle weitere Parameter der Klasse verfallen
\DeclareDefaultOption{}
% Formatangaben für article
\PassOptionsToClass{a4paper}{article}
\PassOptionsToClass{12pt}{article}
% Schlüssel und Optionen verarbeiten
\ProcessKeyvalOptions*
% Sprachangabe wird an babel, csquotes und biblatex weitergegeben
\PassOptionsToPackage{ngerman,\GUFB@language}{babel}
\PassOptionsToPackage{style=\GUFB@language}{csquotes}
\PassOptionsToPackage{language=\GUFB@language}{biblatex}
\newcommand\GUFBdefaultlanguage\GUFB@language
% eigene Titelseite speichern
\savesymbol{maketitle}
\LoadClass{article}
% eigene Titelseite wiederherstellen (\ARTmaketitle für die Titelseite von der article Klasse verwenden)
\restoresymbol{ART}{maketitle}
% Babel laden für die Sprachenunterstützung
\RequirePackage{babel}
% Vermeide nach Möglichkeit Schusterjungen und Hurenkinder
%\widowpenalty=10000
%\clubpenalty=10000
% Für Zitate wird csquotes verwendet
\PassOptionsToPackage{autostyle}{csquotes}
% Zitate länger als 5 Zeilen werden eingerückt
\PassOptionsToPackage{threshold=5}{csquotes}
\RequirePackage{csquotes}
\SetBlockEnvironment{quotation}
% eingerückte Zitate haben einfachen Zeilenabstand, Schriftgröße 10 (entspricht \footnotesize bei allgemeiner Schriftgröße 12) und sind mit Anführungszeichen versehen (benötigt das package csquotes). Die Anführungszeichen werden vor das Zitat gerückt:
%     "Dies ist ein Beispielzitat
%      mit zwei Zeilen
%     ^ Das ist die Distanz von \csq@qopen, die mittels \parindent nach links eingerückt wird
\RequirePackage{calc} % für Längenberechnungen
\newlength{\openquotelength} % neue Länge für öffnende Anführungsstriche
\setlength{\openquotelength}{\widthof{\csq@qopen}} % die Länge von öffnenden Anführungsstrichen aus csquotes berechnen
\renewcommand\mkblockquote[4]{%
%\setlength{\parskip}{-.5em}%#
%\setlength{\parindent}{-\openquotelength}%
\vspace{-1em}%
\indent\singlespacing\footnotesize%
\hspace{-\parindent}\hspace{-\openquotelength}% move first line to the left by paragraph indentation + open quote length
\enquote{#1#2#3}#4%
\vspace{.8em}%
}
% Für die Titelseite wird das Graphicx-Paket benötigt
\RequirePackage{graphicx}
% Für die Positionierung der Grafik verwenden wir adjustbox
\PassOptionsToPackage{export}{adjustbox}
\RequirePackage{adjustbox}
% "Contents" aus dem Inhaltsverzeichnis entfernen
\PassOptionsToPackage{nottoc}{tocbibind}
\RequirePackage{tocbibind}
%
%% Schrift und Text-Formate
% Times New Roman (bzw. dessen offener Ersatz) ist Standardschriftart
\RequirePackage{libertine}
% Der Zeilenabstand muss 1,5-fach sein
\RequirePackage{setspace}
\onehalfspacing%
% Absätze sollen bündig und nicht eingerückt beginnen
\RequirePackage{parskip}
% Größe des Textfeldes (DIN-A4 minus Ränder)
\setlength{\textwidth}{150mm}
\setlength{\textheight}{247mm}
% Seitenränder
\RequirePackage[top=2.5cm, bottom=2.5cm, left=3cm, right=3cm]{geometry}
%
%% Einbinden der Erklärung zur Prüfungsleistung
%\DeclareRobustCommand{\erklpruf}{\input{erklpruf.tex}}
%
% Zwischen draft und nicht-draft unterscheiden
\RequirePackage{ifdraft}
%% Formatierungen für Zitate und Literaturverzeichnis
\input{biblfrmt}%
\SetCiteCommand{\autocite}
