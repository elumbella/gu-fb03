# LaTeX package for writing assignments at the FB03 at the Goethe University Frankfurt
This is a class written for the few people at the department 3 that use LaTeX for their assignments.
It is possible to write a simple assignment (Hausarbeit), a thesis, or even a proposal (Exposé) for that thesis.
This class can be used to write german, english or other works ith minor adjustments (titlepage and "Erklärung zur Prüfungsleistung" will always stay german though to comply with the requirements of the deparment.
Especially the regulations for citing and bibliography have been implemented.
Most publication types should be rendered properly.
Citing is done inline with the ability to specify pages.

So far, this has only been used by me to write a few assignments for the Institute for Political Science (Institut für Politikwissenschaft), but it should also be applicable for the Institute of Sociology.
The logo for the titlepages needs to be added manually as GU-Logo-schwarz.eps which can be downloaded from the University website.
It is not provided here for copyright reasons.

A proper readme or tutorial will maybe come later.
For now, take a close look at the comments.